﻿import './App.css';
import React, { Component } from 'react';
import Film from './Film/film';
import Header from './Header/header';
import Footer from './Footer/footer';

class App extends Component {
    state = {
        films: [
            { name: 'Джокер', image: 'https://st.kp.yandex.net/images/film_iphone/iphone360_1048334.jpg', year: 'Год выпуска : 2019' },
            { name: 'Веном', image: 'https://st.kp.yandex.net/images/film_iphone/iphone360_463634.jpg', year: 'Год выпуска : 2018' },
            { name: 'Форсаж', image: 'http://timemovie.ru/_nw/9/24284028.jpg', year: 'Год выпуска : 2019' },
            { name: 'Алладин', image: 'https://www.kinopoisk.ru/images/film_big/1007049.jpg', year: 'Год выпуска : 2019' }
        ],
        footer: 
            { footer: '&copy; 2019' }
        
    };

    render() {
        return (
            <div className="App">
                <Film name={this.state.films[0].name} image={this.state.films[0].image} year={this.state.films[0].year} />
                <Film name={this.state.films[1].name} image={this.state.films[1].image} year={this.state.films[1].year} />
                <Film name={this.state.films[2].name} image={this.state.films[2].image} year={this.state.films[2].year} />
                <Film name={this.state.films[3].name} image={this.state.films[3].image} year={this.state.films[3].year} />
                <Footer footer={this.state.footer.footer}></Footer>
            </div>
        );
    }

}


export default App;




