import React from 'react';
import './footer.css';


class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <footer>{this.props.footer}</footer>
            </div>
        )
    }
}
export default Footer;

