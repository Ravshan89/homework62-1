import React from 'react';
import './film.css'

class Film extends React.Component {
    render() {
        return (
            <div className="film">
                <h1>{this.props.name}</h1>
                <p><img src={this.props.image}></img></p>
                <p>{this.props.year}</p>
            </div>
        )
    }
}


export default Film;
// JavaScript source code
